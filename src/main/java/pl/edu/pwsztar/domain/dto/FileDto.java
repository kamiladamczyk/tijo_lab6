package pl.edu.pwsztar.domain.dto;

import java.util.List;

public class FileDto {

    private final List<MovieDto> movies;

    private FileDto(Builder builder) {
        this.movies = builder.movies;
    }

    public static Builder Builder() {
        return new Builder();
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    public static final class Builder {
        private List<MovieDto> movies;

        private Builder() {
        }

        public Builder movies(List<MovieDto> movies) {
            this.movies = movies;
            return this;
        }

        public FileDto build() {
            return new FileDto(this);
        }
    }
}
